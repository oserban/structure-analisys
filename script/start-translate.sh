#!/bin/bash

###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###

# author: Ovidiu Serban, ovidiu.serban@cl.cam.ac.uk
# credits: Qiang Guo, Ovidiu Serban, Juan Tirado, Eiko Yoneki  # alphabetic order
# maintainer: Ovidiu Serban


# The start translation script takes one parameter as input, the country code of the target translation
# The translation process is sometimes slow and very error prone, so the script is run in a loop until the entire
# file has been translated. In case the translation service fails too many times, the script can be stopped at any point
# and when restarted will resume the translation process

# The loop has to be preformed this way because of a cleanup problem with the Python library


pythonPath="src/"
pythonExecutionPoint="cam/process/batchFrequencyTranslate.py"
executionParameters=$1

loopCondition=1

while [ ${loopCondition} -eq 1 ]; do
    PYTHONPATH=${pythonPath} python3 -u "${pythonPath}/${pythonExecutionPoint}" ${executionParameters}

    if [ $? -eq 1 ]; then
        loopCondition=1
        echo "Sleeping 2 minutes and then retrying ..."
        sleep 2m
    else
        loopCondition=0
        echo "Finished translation ..."
    fi
done