###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
import json
import logging
import traceback
from glob import glob

from bs4 import BeautifulSoup

from cam.converter.datarescuers import getRescueClass, DataRescue
from cam.dictionary.encodingdictionary import getEncodingDictionary
from cam.metrics.similarities import similarityParentPath
from cam.parse.simpleGenericParsing import GenericParser, getParser
from cam.parse.simpleParsing import fixEmail
from cam.serialisers.fileSerializer import FileSerializer
from cam.settings import ApplicationSettings as settings

try:
    import matplotlib.pyplot as plt
    from cairocffi import *
except:
    raise

import networkx as nx

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("batchCoOccurrenceAnalysis")


###
# This is an experimental class and should not be used
# The dependencies of this file are not part of the virtual environment dependencies
###

def processDocument(_filename: str,
                    _dataRescuer: DataRescue,
                    _dataParser: GenericParser):
    try:
        with codecs.open(_filename, "r+", "utf-8") as ifile:
            fileContent = ifile.read()
            soup = BeautifulSoup(fixEmail(fileContent), 'html5lib')
            if _dataRescuer.isRootChangeable(soup):
                soup = _dataRescuer.changeRoot(soup)
                logging.info("Changed processing root to: " + soup.name)

            _dataParser.traverseDelegate(soup, _addPair, _dataRescuer)
            logger.info("Finished parsing: " + _filename)
    except Exception as e:
        print("Exception: " + str(e))
        traceback.print_exc(file=sys.stdout)


def _addPair(path, text):
    global previousKey

    (currentKey, similarityMeasure) = mappingDictionary.getTextEncodingSimilarity(previousKey, text, tokenThreshold)

    if (currentKey is not None) and (similarityMeasure >= listSimilarityThreshold):
        processOccurrences(currentKey, path)
        documentKeyPath[currentKey] = path
        previousKey = currentKey


def processOccurrences(key: str, path: list()):
    if key in documentKeyPath.keys():
        return

    if key not in allKeys:
        allKeys[key] = len(allKeys)

    # minDiff = len(path)
    maxSimilarity = 0
    bestItem = []
    # if key not in documentKeyPath.keys():
    for (item, value) in documentKeyPath.items():
        itemSimilarityDiff, itemSimilarity = similarityParentPath(value, path)
        if itemSimilarity > maxSimilarity:
            maxSimilarity = itemSimilarity
            bestItem = [item]
            # minDiff = itemSimilarityDiff
        elif itemSimilarity == maxSimilarity:
            bestItem.append(item)
            # if itemSimilarityDiff < minDiff:
            #     bestItem = item
            #     minDiff = itemSimilarityDiff

    if len(bestItem) > 0:
        for item in bestItem:
            updateOccurrence(item, key)


def updateOccurrence(k1: str, k2: str):
    if k1 != k2:
        key = min(k1, k2) + "~" + max(k1, k2)
        # key = k1 + "~" + k2
        count = occurrenceDictionary.get(key, 0)
        count += 1
        occurrenceDictionary[key] = count


def convertToMatrix(_outputFile: str):
    with codecs.open(_outputFile, "w") as oFile:
        oFile.write("U\n")
        oFile.write(str(len(allKeys)) + "\n")

        toRemove = set()

        for (key, value) in occurrenceDictionary.items():
            if value < 2:
                toRemove.add(key)

        for key in toRemove:
            occurrenceDictionary.pop(key, None)

        oFile.write(str(len(occurrenceDictionary)) + "\n")

        for key in occurrenceDictionary.keys():
            kk = key.split("~")
            oFile.write(str(allKeys[kk[0]]) + " " + str(allKeys[kk[1]]) + "\n")


def displayGraph(_outputFile: str):
    G = nx.Graph()

    for (key, value) in occurrenceDictionary.items():
        if value > 1:
            weight = value / numDocs
            kk = key.split("~")
            G.add_edge(kk[0], kk[1], weight=weight)

    elarge = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] > 0.5]
    esmall = [(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] <= 0.5]

    pos = nx.spring_layout(G)  # positions for all nodes

    # nodes
    nx.draw_networkx_nodes(G, pos, node_size=700)

    # edges
    nx.draw_networkx_edges(G, pos, edgelist=elarge, width=6)
    nx.draw_networkx_edges(G, pos, edgelist=esmall, width=6, alpha=0.5, edge_color='b', style='dashed')

    # labels
    nx.draw_networkx_labels(G, pos, font_size=10, font_family='sans-serif')

    plt.axis('off')
    plt.savefig(_outputFile)  # save as png
    plt.show()  # display


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    languageCode = None
    if len(sys.argv) == 3:
        languageCode = sys.argv[2]
        logger.info("Using Language code = " + languageCode)

    countryCode = sys.argv[1]
    encodingFile = "res/{}/encoding.json".format(countryCode)
    inFolder = "res/{}/raw/*".format(countryCode)

    logger.info("Using input folder = %s", inFolder)

    serializer = FileSerializer(encodingFile)
    mappingDictionary = getEncodingDictionary(countryCode)
    mappingDictionary.loadEncoding(serializer)

    dataRescuer = getRescueClass(countryCode)

    corpora_list = glob(inFolder)

    tokenThreshold = mappingDictionary.getTokenThreshold(settings.DEFAULT_tokenThreshold)
    listSimilarityThreshold = mappingDictionary.getListSimilarityThreshold(settings.DEFAULT_listSimilarityThreshold)
    pathThreshold = mappingDictionary.getPathThreshold(settings.DEFAULT_pathThreshold)
    previousKey = None

    occurrenceDictionary = dict()
    allKeys = dict()

    numDocs = 0

    for filename in corpora_list:
        dataParser = getParser(countryCode)
        documentKeyPath = dict()
        processDocument(filename, dataRescuer, dataParser)
        numDocs += 1

        # print(json.dumps(documentKeyPath, sort_keys=True, indent=2))

    print(json.dumps(occurrenceDictionary, sort_keys=True, indent=2))

    # convertToMatrix("res/{}/matrix.mat".format(countryCode))
    displayGraph("res/{}/weighted_graph.png".format(countryCode))
