###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
import logging
import random
import sys

from glob import glob
from bs4 import BeautifulSoup

from cam.parse.simpleGenericParsing import GenericParser, getParser
from cam.dictionary.xpathtextdictionary import XPathTextDictionary
from cam.parse.simpleParsing import fixEmailInDocument
from cam.serialisers.fileSerializer import FileSerializer

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("FrequencyAnalysis")

# the processing sample size can be customised according the the quantity and quality of the input corpora
SAMPLE_SIZE = 100


# This is the main script to compute the frequency analysis on the whole corpora
# Can be launched via the bash script


def computeCorporaEncoding(corpora: list, outFile: str, dataParser: GenericParser):
    dictionary = XPathTextDictionary(True, 0.7)
    fileIndex = 0
    for f in corpora:
        with codecs.open(f, "r+", "utf-8") as ifile:
            soup = BeautifulSoup(fixEmailInDocument(ifile), 'html5lib')
            dataParser.traverseToDictionary(soup, dictionary)
        logger.info("Processed file = %s", f)
        fileIndex += 1
        if fileIndex % 10 == 0:
            saveEncoding(dictionary, outFile)
    return dictionary


def saveEncoding(dictionary, outFile):
    _serializer = FileSerializer(outFile)
    _serializer.cleanup()
    dictionary.save(_serializer)
    _serializer.close()
    logger.info("Encoding file saved = %s", outFile)


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    inFolder = "res/{}/raw/*".format(lang)
    _outFile = "res/{}/data.json".format(lang)

    logger.info("Using input folder = %s", inFolder)
    logger.info("Using output folder = %s", _outFile)

    globList = glob(inFolder)

    _dataParser = getParser(lang)

    corpora_list = random.sample(globList, min(SAMPLE_SIZE, len(globList)))

    logger.info("Using %s random samples ...", len(corpora_list))

    corpora_dict = computeCorporaEncoding(corpora_list, _outFile, _dataParser)
    saveEncoding(corpora_dict, _outFile)
