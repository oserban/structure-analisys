###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import logging
import os
import time
import sys

from microsofttranslator import Translator

from cam.dictionary.xpathtextdictionary import XPathTextDictionary
from cam.serialisers.fileSerializer import FileSerializer
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"

logger = logging.getLogger("FrequencyTranslate")

error = False
TRANSLATION_CLIENT_ID = 'your client id'
TRANSLATION_CLIENT_SECRET = 'your secret'


# The main translation script, which takes the frequency dictionary as input and generates a translation file to
# ease the dictionary review process
# This process is optional
# The TRANSLATION_CLIENT_ID and TRANSLATION_CLIENT_SECRET should be configured with the parameters provided by the
# Microsoft Translate Access tokens

def loadDictionary(dictionaryName):
    _serializer = FileSerializer(dictionaryName)
    dictionary = XPathTextDictionary(True, 0.7)
    dictionary.load(_serializer, None)
    _serializer.close()
    return dictionary


def translate(corporaDict, originalLang):
    translator = Translator(TRANSLATION_CLIENT_ID, TRANSLATION_CLIENT_SECRET)
    for (encoding, synsets) in corporaDict.textEncoding().items():
        if (not corporaDict.hasTranslation(encoding)) and \
                (corporaDict.encodingFrequency(encoding) > settings.PROPERTY_translationFrequencyThreshold):
            for synset in synsets:
                line = "\"" + " ".join(synset) + "\""
                if len(line) > settings.PROPERTY_translationWordThreshold:
                    logger.debug("Translating line: " + line)
                    try:
                        translatedLine = translator.translate(line, "en")
                    except Exception as e:
                        logger.error("Had some error : " + str(e))
                        global error
                        error = True
                        return
                    logger.info("[%s] %s => [EN] %s", originalLang, line, translatedLine)
                    corporaDict.addTranslation(encoding, translatedLine)
                else:
                    logger.info("Skipped line=%s, line too short, engine will raise an error ...", line)
        else:
            logger.info("Skipped key=%s, translation exists or very low frequency ...", encoding)


def saveDictionary(dictionaryName, dictionary):
    mongo_serializer = FileSerializer(dictionaryName)
    dictionary.save(mongo_serializer)
    mongo_serializer.close()


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                        format='%(asctime)s [%(name)s] %(levelname)s %(message)s',
                        datefmt='%a, %d %b %Y %H:%M:%S')

    if len(sys.argv) < 2:
        logger.error("Language must be provided as a parameter ...")
        exit()

    lang = sys.argv[1]
    translatedFile = "res/{}/data.translated.json".format(lang)
    dataFile = "res/{}/data.json".format(lang)

    logger.info("Using language = %s, translatedFilename=%s dataFilename=%s", lang, translatedFile, dataFile)

    tic = time.clock()

    if os.path.isfile(translatedFile):
        _corporaDict = loadDictionary(translatedFile)
        logger.info("Loaded translated file as input ...")
    else:
        _corporaDict = loadDictionary(dataFile)
        logger.info("Loaded data file as input ...")

    translate(_corporaDict, lang)
    saveDictionary(translatedFile, _corporaDict)
    print(time.clock() - tic)

    if error:
        sys.exit(1)
    else:
        sys.exit(0)
