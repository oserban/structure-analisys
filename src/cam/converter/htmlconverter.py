###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###

import codecs
import logging

from bs4 import BeautifulSoup, Tag

from cam.converter.xmlcreator import GenericXmlCreator
from cam.parse.simpleGenericParsing import GenericParser
from cam.converter.datarescuers import DataRescue
from cam.dictionary.encodingdictionary import EncodingDictionary
from cam.metrics.similarities import similarity_path
from cam.parse.simpleParsing import fixEmail

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class HtmlConverter:
    """
    This is the main HTML to XML conversion library
    It uses manny additional parameters and helpers in the conversion process:

    - the mapping dictionary, which serves as the model for the key encodings
    - various thresholds used in the comparison process
    - the data rescue helper class, used to threat all the non-standard cases or where the processing fails
    - data parser, describes the iteration strategy of the xml data, either Flat or Hierachical
    - xml creator, describes the xml creation strategy, flat or hierarchical
    - language code (or country code) of the original file
    """

    def __init__(self, mappingDictionary: EncodingDictionary,
                 tokenThreshold: float, listSimilarityThreshold: float, pathThreshold: float,
                 dataRescuer: DataRescue, dataParser: GenericParser, xmlCreator: GenericXmlCreator, language: str):
        self._mappingDictionary = mappingDictionary
        self._tokenThreshold = tokenThreshold
        self._listSimilarityThreshold = listSimilarityThreshold
        self._pathThreshold = pathThreshold
        self._dataRescuer = dataRescuer
        self._dataParser = dataParser
        self._xmlCreator = xmlCreator
        self._language = language

        logging.info("HTML Converter using the parameters, tokenThreshold = %s, listSimilarityThreshold = %s, "
                     "pathThreshold = %s", tokenThreshold, listSimilarityThreshold, pathThreshold)

        self._previousKey = "Root"
        self._previousPath = []

        self._textBuffer = ""

    def convertFilename(self, filename: str):
        with codecs.open(filename, "r+", "utf-8") as ifile:
            fileContent = ifile.read()
            self.convertFileContent(fileContent, filename)

    def convertFileContent(self, fileContent: str, filename: str):
        soup = BeautifulSoup(fixEmail(fileContent), 'html5lib')
        self.convertSoup(soup, filename)

    def convertSoup(self, soup: Tag, filename: str):
        rootDocument = soup

        if self._dataRescuer.canProcessNonStandard():
            nonStandardResult = self._dataRescuer.parseNonStandard(soup)
            if nonStandardResult is not None and len(nonStandardResult) > 0:
                self._xmlCreator.storeDictionary(nonStandardResult)

        if self._dataRescuer.isRootChangeable(rootDocument):
            rootDocument = self._dataRescuer.changeRoot(rootDocument)
            logging.info("Changed processing root to: " + rootDocument.name)

        self._dataParser.traverseDelegate(rootDocument, self._addPair, self._dataRescuer)

        logging.info("Finished processing filename: %s !", filename)

    def _addPair(self, path, text):
        joinedPath = "/".join(path) + "/" + text

        (currentKey, similarityMeasure) = self._mappingDictionary.getTextEncodingSimilarity(self._previousKey, text,
                                                                                            self._tokenThreshold)
        if (currentKey is None) or (similarityMeasure < self._listSimilarityThreshold):
            if similarity_path(self._previousPath, path) <= self._pathThreshold:
                # seems to be a value
                logging.debug(" MATCHED VAL: %s = %s (%s)", self._previousKey, text, joinedPath)
                self._textBuffer = self._textBuffer + " " + text
            else:
                self._flushTextBuffer()
                if self._dataRescuer.isTextRescuable(path, text):
                    (currentKey, rescuedText) = self._dataRescuer.rescueText(path, text)
                    if currentKey is not None and rescuedText is not None and len(rescuedText) > 0:
                        self._previousKey = currentKey
                        self._previousPath = path
                        self._textBuffer = self._textBuffer + " " + rescuedText
                else:
                    logging.debug("IGNORED TEXT: %s (%s)", text, joinedPath)
        else:
            if self._mappingDictionary.isIgnore(currentKey):
                self._flushTextBuffer()
                logging.debug(" IGNORED KEY: %s (%s)", text, joinedPath)
            else:
                self._flushTextBuffer()
                logging.debug(" MATCHED KEY: %s = %s (%s)", currentKey, text, joinedPath)
                self._previousKey = currentKey
                self._previousPath = path

    def _flushTextBuffer(self):
        if len(self._textBuffer) > 0:
            self._xmlCreator.storeKey(self._dataRescuer, self._previousKey, self._textBuffer, self._language)
            self._textBuffer = ""
