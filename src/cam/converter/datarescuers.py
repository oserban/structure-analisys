###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###

from bs4 import Tag

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class DataRescue:
    """
    Data Rescue is a mechanism to salvage any HTML tags that cannot be processed automatically
    This is data dependent and should be written once
    """

    def __init__(self):
        pass

    def rescueTag(self, tag: Tag) -> str:
        """
        If the tag can be rescued (based on the programmer's rules), the rescued value can be returned
        This method is called during xpath iteration, before the in depth traversal

        :param tag: a Beautiful Soup Tag element
        :returns a string, the rescued value of the tag
        """
        pass

    def isTagRescuable(self, tag: Tag) -> bool:
        """
        Called before a tag rescue

        :param tag: a Beautiful Soup Tag element
        :returns bool, true if the tag can be rescued, false otherwise
        """
        return False

    def changeRoot(self, tag: Tag) -> Tag:
        """
        For certain documents, the processing root needs to be changed to limit the processing noise
        If the root can be changed, the new root should be returned.

        This method is called before any processing is done on the HTML document

        :param tag: a Beautiful Soup Tag element, usually the root of the original document
        :returns a Beautiful Soup Tag element, representing the new root of the document
        """
        return tag

    def isRootChangeable(self, tag):
        """
        Called before the change root method

        :param tag: a Beautiful Soup Tag element, usually the root of the original document
        :returns bool, true is the root can be changed, false otherwise
        """
        return False

    def parseNonStandard(self, tag: Tag) -> dict():
        """
        Certain documents contain elements that need to be converted manually,
        without using the automatic processing rules

        This method is called before the root is changed and before the processing starts

        :param tag: a Beautiful Soup Tag element, usually the root of the original document
        :returns a dictionary containing the processed non-standard elements
        """
        return None

    def canProcessNonStandard(self):
        """
        Called before the parse non standard method

        :returns bool, true if non standard elements can be processed, false otherwise
        """
        return False

    def rescueText(self, path: list, text: str) -> tuple((str, str)):
        """
        Usually the text values should be parsed correctly by the automatic algorithm, if the dictionary is
        generated properly. Nevertheless, for certain values the processing fails due to a number of reasons.
        By default, the title of the document and highlighted section will be rescued.

        This method is called at the end of the processing process, if the text value has not been mapped to any key

        :param path: the xpath of the element
        :param text: the text of the element
        :returns tuple containing the name of the key, value of the element, or None if it can't be rescued
        """
        for tag in ["title", "h1", "h2", "h3"]:
            if tag in path:
                return tag, text
        return None

    def isTextRescuable(self, path, text):
        """
        Called before the rescue text method

        :param path: the xpath of the element
        :param text: the text of the element
        :returns bool, true if path and text can be rescued, false otherwise
        """
        for tag in ["title", "h1", "h2", "h3"]:
            if tag in path:
                return True
        return False

    def cleanupText(self, text):
        """
        Final text cleanup for the values, used to remove prefixes and suffixes

        This method is called at the end of the processing process

        :param text: the text of the element
        :returns the cleaned text
        """
        return text


# These are some Data Rescuers implementations, based on the production rules identified

class CzRescuer(DataRescue):
    def rescueTag(self, tag):
        if tag.name == "fieldset":
            return self._rescueFieldset(tag)
        elif tag.name == "input":
            return self._rescueInputText(tag)
        elif tag.name == "select":
            return self._rescueSelectValue(tag)

    def isTagRescuable(self, tag):
        if tag.name == "fieldset" and "class" in tag.attrs and "form" in tag.attrs.get("class"):
            for itemType in ["checkbox", "radio"]:
                itemTypeList = tag.findAll("input", {"type": itemType})
                if itemTypeList is not None and len(itemTypeList) > 0:
                    return True
        elif tag.name == "input" and "class" in tag.attrs and "input-readonly" in tag.attrs.get("class") and \
                        "type" in tag.attrs and "text" in tag.attrs.get("type") and "value" in tag.attrs:
            return True
        elif tag.name == "select":
            return True
        return False

    @staticmethod
    def _rescueFieldset(tag):
        result = []

        for itemType in ["checkbox", "radio"]:
            itemTypeList = tag.findAll("input", {"type": itemType})
            if itemTypeList is not None:
                for inputItem in itemTypeList:
                    if inputItem.attrs.get("checked") is not None and "checked" in inputItem.attrs.get("checked"):
                        inputItemId = inputItem.attrs.get("id")
                        labels = tag.findAll("label", {"for": inputItemId})
                        if labels is not None:
                            for labelItem in labels:
                                result.append(labelItem.text)

        return ";".join(result)

    @staticmethod
    def _rescueInputText(tag):
        inputValue = tag.attrs.get("value")
        if inputValue is not None:
            return inputValue
        else:
            return ""

    @staticmethod
    def _rescueSelectValue(tag):
        result = []
        selectedValues = tag.findAll("option", {"selected": "selected"})
        if selectedValues is not None:
            for selectedValue in selectedValues:
                result.append(selectedValue.text)
        return ";".join(result)

    def cleanupText(self, text: str):
        if text is not None:
            return text.strip("*").strip()
        else:
            return None


class PtRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"template": "base/pt/Pesquisa/Contrato.php"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        newRoots = tag.findAll("div", {"template": "base/pt/Pesquisa/Anuncio.php"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag


class LtRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"class": "content"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag


class EeRescuer(DataRescue):
    def isTextRescuable(self, path, text):
        if self._isContractType(path, text):
            return True
        return super().isTextRescuable(path, text)

    def rescueText(self, path, text):
        if self._isContractType(path, text):
            return "contractType", text
        else:
            return super().rescueText(path, text)

    @staticmethod
    def _isContractType(path, text):
        if len(path) > 0 and path[-1] == "font":
            for item in ["Riigihanke aruanne", "Hanketeade"]:
                if item in text:
                    return True
        return False


class ChRescuer(DataRescue):
    def isRootChangeable(self, tag):
        return True

    def changeRoot(self, tag):
        newRoots = tag.findAll("div", {"class": "preview"})
        if newRoots is not None and len(newRoots) > 0:
            return newRoots[0]
        return tag

    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "result_head"})
        if headerTag is not None and len(headerTag) > 0:
            headerTagSplit = headerTag[0].text.split("|")
            result["publicationDatePortal"] = headerTagSplit[0]
            result["projectId"] = headerTagSplit[1].split(" ")[-1]
            result["announcementId"] = headerTagSplit[2].split(" ")[-1]
            result["announcementType"] = headerTagSplit[3]

        return result


class BgRescuer(DataRescue):
    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "stdoc"})
        if headerTag is not None and len(headerTag) > 0:
            counter = 0
            for item in headerTag:
                result["stdoc-line-" + str(counter)] = item.text.strip()
                counter += 1
        return result

    def isTextRescuable(self, path, text):
        if self._isContractNumber(path, text):
            return True
        else:
            return super().isTextRescuable(path, text)

    def rescueText(self, path, text):
        if self._isContractNumber(path, text):
            return "contractNumber", text
        else:
            return super().rescueText(path, text)

    @staticmethod
    def _isContractNumber(path, text):
        if len(path) > 1 and path[-1] == "p" and path[-2] == "font":
            return True
        return False


class HrRescuer(DataRescue):
    def isTagRescuable(self, tag):
        if self._ignoreDiv(tag):
            return True
        else:
            return super().isTagRescuable(tag)

    def rescueTag(self, tag):
        if self._ignoreDiv(tag):
            return ""
        else:
            return super().rescueTag(tag)

    def canProcessNonStandard(self):
        return True

    def parseNonStandard(self, tag):
        result = dict()

        headerTag = tag.findAll("div", {"class": "FormHeader"})
        if headerTag is not None and len(headerTag) > 0:
            result["announcementType"] = headerTag[0].text.strip()
            if len(result["announcementType"]) == 0:
                result["announcementType"] = "Objava - Koncesije"
        return result

    @staticmethod
    def _ignoreDiv(tag):
        if tag.name == "div" and "class" in tag.attrs and "FormHeader" in tag.attrs.get("class"):
            return True
        else:
            return False


# custom rescuers, based on some country codes

rescuers = {
    "bg": BgRescuer,
    "ch-fr": ChRescuer,
    "ch-it": ChRescuer,
    "ch-en": ChRescuer,
    "ch-de": ChRescuer,
    "cz": CzRescuer,
    "ee": EeRescuer,
    "hr": HrRescuer,
    "pt": PtRescuer,
    "lt": LtRescuer
}


def getRescueClass(countryCode):
    rescuer = rescuers.get(countryCode, DataRescue)
    return rescuer()
