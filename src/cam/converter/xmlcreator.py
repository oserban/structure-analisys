###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import codecs
from xml.etree.ElementTree import Element, SubElement
from xml.etree import ElementTree
from xml.dom import minidom

from cam.converter.datarescuers import DataRescue
from cam.parse.simpleParsing import splitKey, sameParent

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class GenericXmlCreator:
    """
    This is helper base class used to store the xml structure and it is dependent upon the key structure,
    being either flat or hierarchical.
    """

    def __init__(self):
        self.documentElement = Element("document")

    def storeDictionary(self, propertyDictionary: dict):
        for key, value in propertyDictionary.items():
            child = SubElement(self.documentElement, 'child')
            child.text = value.strip()

    def storeKey(self, dataRescuer: DataRescue, key: str, text: str, language: str):
        pass

    def _flushKeysBeforeClose(self):
        pass

    def saveFilename(self, outputFilename: str):
        self._flushKeysBeforeClose()

        with codecs.open(outputFilename, "w+", "utf-8") as iFile:
            iFile.write(self._prettify(self.documentElement))

    @staticmethod
    def _generateKeyName(key: str, language: str):
        if language is None:
            return "k" + key
        else:
            return "k_" + language + "_" + key

    @staticmethod
    def _prettify(elem: Element):
        rough_string = ElementTree.tostring(elem, 'utf-8')
        parsed = minidom.parseString(rough_string)
        return parsed.toprettyxml(indent="  ", newl="\n", encoding='utf-8').decode('utf-8')


class FlatXmlCreator(GenericXmlCreator):
    """
    Implementation of the Flat XML creation strategy
    """

    def __init__(self):
        super().__init__()

    def storeKey(self, dataRescuer: DataRescue, key: str, text: str, language: str):
        localKey = key.split(".")
        self._recFlushText(self.documentElement, localKey, dataRescuer.cleanupText(text.strip()), language)

    def _recFlushText(self, parent: Element, keyList: list, text: str, language: str):
        if len(keyList) > 0:
            keyListHead = keyList.pop(0)
            child = SubElement(parent, self._generateKeyName(keyListHead, language))
            self._recFlushText(child, keyList, text, language)
        else:
            parent.text = text


class HierarchicalXmlCreator(GenericXmlCreator):
    """
    Hierarchical implementation of the XML data creation

    The key has the following format: parent.parent.child, with the parent not restricted to any number of levels
    """

    def __init__(self):
        super().__init__()
        self.keyCache = []

    def _hasKey(self, key):
        result = [item for item in self.keyCache if item[0] == key]
        return len(result) > 0

    def storeKey(self, dataRescuer, key, text, language):
        if len(self.keyCache) > 0:
            (lkey, ltext, llanguage) = self.keyCache[(len(self.keyCache) - 1)]
            if self._hasKey(key) or not sameParent(key, lkey):
                self._flushKeysBeforeClose()

        self.keyCache.append((key, dataRescuer.cleanupText(text.strip()), language))

    def _flushKeysBeforeClose(self):
        sKeyCache = sorted(self.keyCache, key=lambda x: x[0])
        self.keyCache.clear()

        createdElements = dict()

        for (key, text, language) in sKeyCache:
            self._createSimpleKey(createdElements, key, text, language)

    def _createSimpleKey(self, createdElements: dict, key: str, text: str, language: str):
        elementKey = createdElements.get(key, None)
        if elementKey is None:
            (pKey, cKey) = splitKey(key)
            elementKey = SubElement(self._getParentElement(createdElements, pKey, language),
                                    self._generateKeyName(cKey, language))
            createdElements[key] = elementKey
        # we do this because the key may be artificially created by another call
        elementKey.text = text

    def _getParentElement(self, createdElements: dict, key: str, language: str):
        if key is None:
            return self.documentElement
        else:
            elementKey = createdElements.get(key, None)
            if elementKey is None:
                (pKey, cKey) = splitKey(key)
                elementKey = SubElement(self._getParentElement(createdElements, pKey, language),
                                        self._generateKeyName(cKey, language))
                createdElements[key] = elementKey
            return elementKey


# custom XML creators, for some country codes

customXmlCreators = {
    "cz": HierarchicalXmlCreator
}


def getXmlCreator(countryCode: str) -> GenericXmlCreator:
    creator = customXmlCreators.get(countryCode, FlatXmlCreator)
    return creator()
