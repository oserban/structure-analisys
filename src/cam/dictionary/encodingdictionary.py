###
# Structure Analysis Project (c) by $Authors
#
# This file is part of the Structure Analysis Project, 24/02/2016
#
# Structure Analysis Project is licensed under a dual license: European Union Public License 1.1
# and Commons Attribution 4.0 International (CC BY 4.0) License.
# Academic institutions and research are required to comply with
# CC BY 4.0 Attribution clause and cite the following paper describing the work.
# Please check the license file for more details.
###


import json
from collections import defaultdict

from cam.metrics.similarities import len_similarity_sets, similarity_sets
from cam.parse.simpleParsing import regexpTokenize, splitKey
from cam.settings import ApplicationSettings as settings

__author__ = "Ovidiu Serban"
__credits__ = ["Qiang Guo", "Ovidiu Serban", "Juan Tirado", "Eiko Yoneki"]  # alphabetic order
__maintainer__ = "Ovidiu Serban"
__email__ = "ovidiu.serban@cl.cam.ac.uk"


class EncodingDictionary:
    """
    Base implementation of the Encoding Dictionary, used at runtime
    """

    def __init__(self):
        self._encoding = defaultdict(set)
        self._ignoreSet = set()

        self._pathThreshold = None
        self._tokenThreshold = None
        self._listSimilarityThreshold = None

    def loadEncoding(self, serializer):
        # load representation
        representation = serializer.deserialize_object(None)

        self.buildKeyModel(representation["keySet"])

        self._ignoreSet = set([str(element) for element in representation["ignoreSet"]])
        self._encoding = representation["encoding"]

        self._pathThreshold = representation.get("pathThreshold", None)
        if self._pathThreshold is not None:
            self._pathThreshold = int(self._pathThreshold)

        self._tokenThreshold = representation.get("tokenThreshold", None)
        if self._tokenThreshold is not None:
            self._tokenThreshold = float(self._tokenThreshold)

        self._listSimilarityThreshold = representation.get("listSimilarityThreshold", None)
        if self._listSimilarityThreshold is not None:
            self._listSimilarityThreshold = float(self._listSimilarityThreshold)

        self._filterUnusedKeys()

    def buildKeyModel(self, keyRepresentation):
        pass

    def getCandidateKeys(self, currentKey):
        return []

    def getAllCandidateKeys(self) -> list(str()):
        return self.getCandidateKeys(None)

    def getEncodingKey(self, currentKey: str) -> str:
        return currentKey

    def generateCandidateKeyValues(self, currentKey):
        for key in self.getCandidateKeys(currentKey):
            value = self._encoding.get(self.getEncodingKey(key), None)
            if value is not None:
                yield (key, value)

    def _filterUnusedKeys(self):
        # filter unused keys
        _allKeys = set(self._encoding.keys())
        _allKeys.difference_update(self.getAllCandidateKeys())
        _allKeys.difference_update(self._ignoreSet)
        for key in _allKeys:
            del self._encoding[key]

    def isIgnore(self, encoding):
        return encoding in self._ignoreSet

    def getPathThreshold(self, defaultValue):
        if self._pathThreshold is not None:
            return self._pathThreshold
        else:
            return defaultValue

    def getTokenThreshold(self, defaultValue):
        if self._tokenThreshold is not None:
            return self._tokenThreshold
        else:
            return defaultValue

    def getListSimilarityThreshold(self, defaultValue):
        if self._listSimilarityThreshold is not None:
            return self._listSimilarityThreshold
        else:
            return defaultValue

    def getTextEncodingSimilarity(self, currentKey, text, tokenSimilarityThreshold):
        textList = tuple(regexpTokenize(text.lower()))
        if len(textList) > settings.PROPERTY_max_tokens:
            return None, 0.0

        maxKey = None
        maxSimilarity = 0

        for (key, values) in self.generateCandidateKeyValues(currentKey):
            for value in values:
                if len(value) <= settings.PROPERTY_max_tokens:
                    if textList == value:
                        # perfect match, should not continue
                        return key, 1.0
                    elif len_similarity_sets(textList, value) <= settings.DEFAULT_listSimilarityThreshold:
                        currentSimilarity = similarity_sets(textList, value, tokenSimilarityThreshold)
                        if currentSimilarity > maxSimilarity:
                            maxSimilarity = currentSimilarity
                            maxKey = key

        return maxKey, maxSimilarity

    @staticmethod
    def _convertSetDictionary(setDictionary):
        listDictionary = defaultdict(list)
        for (key, values) in setDictionary.items():
            for value in values:
                listDictionary[key].append(list(value))
        return listDictionary

    def toString(self):
        return json.dumps(self._convertSetDictionary(self._encoding), sort_keys=True, indent=2)


class FlatEncodingDictionary(EncodingDictionary):
    def __init__(self):
        super().__init__()
        self._keySet = set()

    def buildKeyModel(self, keyRepresentation):
        self._keySet = set([str(element) for element in keyRepresentation])

    def getCandidateKeys(self, currentKey):
        return self._keySet


class HierarchicalEncodingDictionary(EncodingDictionary):
    noneKey = "~"
    allKeys = "*"

    def __init__(self):
        super().__init__()
        self._keySet = defaultdict(list)

    def buildKeyModel(self, keyRepresentation):
        localKeySet = defaultdict(set)
        for key in keyRepresentation:
            self._addKey(key, localKeySet)

        for key in localKeySet.keys():
            self._addParentKey(key, localKeySet)

        self._keySet = self._convertSetDictionary(localKeySet)

    @staticmethod
    def _convertSetDictionary(setDictionary):
        listDictionary = defaultdict(list)
        for (key, values) in setDictionary.items():
            listDictionary[key] = list(values)
        return listDictionary

    def _addKey(self, currentKey, keySet: defaultdict(set)):
        if currentKey is not None and currentKey is not self.noneKey:
            (parentKey, childKey) = self.parseKey(currentKey)
            keySet[self.allKeys].add(childKey)
            keySet[parentKey].add(currentKey)
            keySet[currentKey].add(currentKey)
            self._addKey(parentKey, keySet)

    def _addParentKey(self, currentKey, keySet: defaultdict(set)):
        if currentKey is not None and currentKey is not self.noneKey:
            (parentKey, childKey) = self.parseKey(currentKey)
            keySet[currentKey].update(keySet[parentKey])

            while parentKey is not self.noneKey:
                (parentKey, childKey) = self.parseKey(parentKey)
                keySet[currentKey].update(keySet[parentKey])

    def getCandidateKeys(self, currentKey):
        if currentKey is None:
            return self._keySet[self.noneKey]
        else:
            values = self._keySet[currentKey]
            if len(values) > 0:
                return values
            else:
                return self._keySet[self.noneKey]

    def getAllCandidateKeys(self):
        return self._keySet[self.allKeys]

    def getEncodingKey(self, currentKey):
        (parentKey, key) = self.parseKey(currentKey)
        return key

    def parseKey(self, key: str) -> (str, str):
        """
        Parses the key
        :param key: the current key to be parses
        :return: (parentKey, Key), parentKey can be none, and replaced with the placeholder *
        """
        (parentKey, childKey) = splitKey(key)
        if parentKey is None:
            parentKey = self.noneKey

        return parentKey, childKey


customEncodingDictionary = {
    "cz": HierarchicalEncodingDictionary
}


def getEncodingDictionary(countryCode) -> EncodingDictionary:
    encodingDictionary = customEncodingDictionary.get(countryCode, FlatEncodingDictionary)
    return encodingDictionary()
