MUDE Project (c) by Ovidiu Serban

MUDE Project is licensed under a dual license: *European Union Public License 1.1* and *Commons Attribution 4.0 International (CC BY 4.0)* License.
Research and academic institutions are required to comply with CC BY 4.0 Attribution clause and cite the following paper describing the work, in any publication made using this project:

    Authors, title of paper, publication, etc.


You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by/4.0/> and
<https://joinup.ec.europa.eu/community/eupl/og_page/european-union-public-licence-eupl-v11>.